import 'dart:developer';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:expense_manager/pages/category_month_transaction_graph_page.dart';
import 'package:provider/provider.dart';

import 'package:expense_manager/controllers/graph_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pie_chart/pie_chart.dart';

class GraphPage extends StatefulWidget {
  const GraphPage({super.key});

  @override
  State<GraphPage> createState() => _GraphPageState();
}

class _GraphPageState extends State<GraphPage> {
  @override
  void initState() {
    Provider.of<GraphPageConrtoller>(context, listen: false)
        .setCategoryExpenseMap(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        return Provider.of<GraphPageConrtoller>(context, listen: false)
            .setCategoryExpenseMap(context);
      },
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        children: [
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return const MyPieChart();
            },
          ),
          const SizedBox(
            height: 30,
          ),
          const Divider(),
          const SizedBox(
            height: 20,
          ),
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return getSingleContainer(
                text: "Food",
                category: 1,
                expense:
                    Provider.of<GraphPageConrtoller>(context).food.toString(),
                color: const Color.fromRGBO(214, 3, 3, 0.7),
                icon: Icons.food_bank_outlined,
              );
            },
          ),
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return getSingleContainer(
                text: "Fuel",
                category: 2,
                expense:
                    Provider.of<GraphPageConrtoller>(context).fuel.toString(),
                color: const Color.fromRGBO(241, 38, 196, 0.7),
                icon: Icons.local_gas_station_rounded,
              );
            },
          ),
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return getSingleContainer(
                text: "Medicine",
                category: 3,
                expense: Provider.of<GraphPageConrtoller>(context)
                    .medicine
                    .toString(),
                color: const Color.fromRGBO(0, 174, 91, 0.7),
                icon: Icons.medical_services_outlined,
              );
            },
          ),
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return getSingleContainer(
                text: "Entertainment",
                category: 4,
                expense: Provider.of<GraphPageConrtoller>(context)
                    .entertainment
                    .toString(),
                color: const Color.fromRGBO(100, 62, 255, 0.7),
                icon: Icons.movie_creation_outlined,
              );
            },
          ),
          Consumer<GraphPageConrtoller>(
            builder: (context, value, child) {
              return getSingleContainer(
                text: "Shopping",
                category: 5,
                expense: Provider.of<GraphPageConrtoller>(context)
                    .shoping
                    .toString(),
                color: const Color.fromRGBO(0, 148, 255, 0.7),
                icon: Icons.shopping_bag_outlined,
              );
            },
          ),
          const SizedBox(
            height: 20,
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 40),
            child: Consumer<GraphPageConrtoller>(
              builder: (context, value, child) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      "₹ ${Provider.of<GraphPageConrtoller>(context).total}",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Widget getSingleContainer({
    required String text,
    required IconData icon,
    required Color color,
    required String expense,
    required int category,
  }) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return CategoryMonthTransactionGraphPage(category: category);
                },
              ),
            );
          },
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: color,
                ),
                padding: const EdgeInsets.all(8),
                child: Icon(
                  icon,
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                text,
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 18,
                ),
              ),
              const Spacer(),
              Text(
                "₹ $expense",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              const Icon(
                Icons.arrow_forward_ios_rounded,
                size: 20,
                color: Colors.grey,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class MyPieChart extends StatelessWidget {
  const MyPieChart({super.key});

  @override
  Widget build(BuildContext context) {
    Map<String, double> dataMap = {
      "Food": Provider.of<GraphPageConrtoller>(context).food,
      "Fuel": Provider.of<GraphPageConrtoller>(context).fuel,
      "Medical": Provider.of<GraphPageConrtoller>(context).medicine,
      "Entertainment": Provider.of<GraphPageConrtoller>(context).entertainment,
      "Shopping": Provider.of<GraphPageConrtoller>(context).shoping,
    };
    return PieChart(
      dataMap: dataMap,
      chartType: ChartType.ring,
      ringStrokeWidth: 40,
      chartRadius: 180,
      chartValuesOptions: const ChartValuesOptions(
        showChartValues: false,
      ),
      legendOptions: LegendOptions(
        legendShape: BoxShape.circle,
        legendTextStyle: GoogleFonts.poppins(
          fontSize: 15,
          fontWeight: FontWeight.w500,
        ),
      ),
      animationDuration: const Duration(seconds: 2),
      colorList: const [
        Color.fromRGBO(214, 3, 3, 0.7),
        Color.fromRGBO(241, 38, 196, 0.7),
        Color.fromRGBO(0, 174, 91, 0.7),
        Color.fromRGBO(100, 62, 255, 0.7),
        Color.fromRGBO(0, 148, 255, 0.7),
      ],
      centerWidget: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Total",
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w400,
              fontSize: 14,
            ),
          ),
          Text(
            "₹ ${Provider.of<GraphPageConrtoller>(context).total}",
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w600,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }
}
