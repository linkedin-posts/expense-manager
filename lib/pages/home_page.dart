import 'dart:developer';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:expense_manager/home_page_widgets/floating_action_button_home_page.dart';
import 'package:expense_manager/home_page_widgets/home_page_drawer.dart';
import 'package:expense_manager/pages/about_us_page.dart';
import 'package:expense_manager/pages/category_page.dart';
import 'package:expense_manager/pages/graph_page.dart';
import 'package:expense_manager/pages/transaction_page.dart';
import 'package:expense_manager/pages/trash_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  final List<Widget> pages = const [
    TransactionPage(),
    GraphPage(),
    Categorypage(),
    TrashPage(),
    AboutUsPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(14, 161, 125, 0.70),
        elevation: 10,
        title: Consumer<HomePageController>(
          builder: (context, value, child) {
            return Text(
              Provider.of<HomePageController>(context).monthName,
              style: GoogleFonts.poppins(
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
            );
          },
        ),
        actions: [
          IconButton(
            onPressed: () {
              Provider.of<HomePageController>(context, listen: false)
                  .changeMonth(context);
            },
            icon: const Icon(
              Icons.keyboard_arrow_down_outlined,
              size: 40,
            ),
          ),
        ],
      ),
      drawer: const HomePageDrawer(),
      backgroundColor: Colors.white,
      body: Consumer<HomePageController>(
        builder: (context, value, child) {
          return WillPopScope(
            onWillPop: () async {
              if (Provider.of<HomePageController>(
                    context,
                    listen: false,
                  ).index ==
                  0) {
                bool? ans = await showDialog<bool>(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      content: Text(
                        "Do you want to Exit App ?",
                        style: TextStyle(
                          color: const Color.fromRGBO(14, 161, 125, 1),
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      backgroundColor: Colors.white,
                      actions: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor:
                                      const Color.fromRGBO(14, 161, 125, 0.70),
                                  elevation: 3,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor:
                                      const Color.fromRGBO(14, 161, 125, 0.70),
                                  elevation: 3,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    );
                  },
                );

                return ans ?? false;
              } else {
                Provider.of<HomePageController>(context, listen: false)
                    .changeIndex(0);
                return false;
              }
            },
            child: pages[
                Provider.of<HomePageController>(context, listen: false).index],
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Consumer<HomePageController>(
        builder: (context, value, child) {
          return value.index == 0
              ? const FloatingActionButtonHomePage()
              : const SizedBox();
        },
      ),
    );
  }
}
