import 'package:expense_manager/controllers/trash_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class TrashPage extends StatelessWidget {
  const TrashPage({super.key});

  @override
  Widget build(BuildContext context) {
    Provider.of<TrashPageController>(context, listen: false).getList(context);
    return Consumer<TrashPageController>(
      builder: (context, value, child) {
        return RefreshIndicator(
          onRefresh: () async {
            return Provider.of<TrashPageController>(
              context,
              listen: false,
            ).getList(context);
          },
          child: ListView.builder(
            padding: const EdgeInsets.only(top: 10),
            itemCount: Provider.of<TrashPageController>(context)
                .transactionList
                .length,
            itemBuilder: (context, index) {
              return TrashPageContainer(
                transaction: Provider.of<TrashPageController>(context)
                    .transactionList[index],
              );
            },
          ),
        );
      },
    );
  }
}

class TrashPageContainer extends StatelessWidget {
  final SingleTransaction transaction;
  const TrashPageContainer({required this.transaction, super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 70,
              child: IconButton(
                onPressed: () async {
                  bool? ans = await showDialog<bool>(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        content: Text(
                          "Do you want to delete this Transaction Permanently ?",
                          style: TextStyle(
                            color: const Color.fromRGBO(14, 161, 125, 1),
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        backgroundColor: Colors.white,
                        actions: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color.fromRGBO(
                                        14, 161, 125, 0.70),
                                    elevation: 3,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color.fromRGBO(
                                        14, 161, 125, 0.70),
                                    elevation: 3,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                  child: Text(
                                    "No",
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      );
                    },
                  );

                  if (ans == true) {
                    Provider.of<TrashPageController>(context, listen: false)
                        .deleteFromTrash(transaction.id, context);
                  }
                },
                icon: Icon(
                  Icons.delete_forever,
                  size: 40,
                  color: Colors.red,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          getCategory(transaction.category),
                          style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        const Spacer(),
                        Text(
                          "300",
                          style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Text(
                      transaction.description,
                      textAlign: TextAlign.justify,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        transaction.datetime,
                        style: GoogleFonts.poppins(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        const Divider(),
      ],
    );
  }

  String getCategory(int category) {
    if (category == 1) {
      return "Food";
    } else if (category == 2) {
      return "Fuel";
    } else if (category == 3) {
      return "Mediciene";
    } else if (category == 4) {
      return "Entertainment";
    } else if (category == 5) {
      return "Shopping";
    } else {
      return "Other";
    }
  }
}
