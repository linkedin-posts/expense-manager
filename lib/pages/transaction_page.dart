import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:expense_manager/controllers/transaction_page_controller.dart';
import 'package:expense_manager/common/single_container_home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TransactionPage extends StatefulWidget {
  const TransactionPage({super.key});

  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<TransactionPageController>(context, listen: false)
          .getList(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TransactionPageController>(
      builder: (context, transactionController, child) {
        return RefreshIndicator(
          onRefresh: () async {
            return Provider.of<TransactionPageController>(context,
                    listen: false)
                .getList(context);
          },
          child: ListView.builder(
            padding: const EdgeInsets.only(top: 10),
            itemCount: transactionController.transactionList.length,
            itemBuilder: (context, index) {
              return SingleContainerHomePage(
                transaction: transactionController.transactionList[index],
              );
            },
          ),
        );
      },
    );
  }
}
