import 'package:expense_manager/controllers/graph_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CategoryMonthTransactionGraphPage extends StatefulWidget {
  final int category;
  const CategoryMonthTransactionGraphPage({required this.category, super.key});

  @override
  State<CategoryMonthTransactionGraphPage> createState() =>
      _CategoryMonthTransactionPageState();
}

class _CategoryMonthTransactionPageState
    extends State<CategoryMonthTransactionGraphPage> {
  Color getColor(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    return colors[category - 1];
  }

  Widget getIcon(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    List<IconData> icons = [
      Icons.food_bank_outlined,
      Icons.local_gas_station_rounded,
      Icons.medical_services_outlined,
      Icons.movie_creation_outlined,
      Icons.shopping_bag_outlined,
    ];

    return Icon(
      icons[category - 1],
      color: Colors.white,
      size: 35,
    );
  }

  String getCategoryName(int category) {
    List<String> list = [
      "Food",
      "Fuel",
      "Medicine",
      "Entertainment",
      "Shopping",
    ];
    return list[category - 1];
  }

  @override
  void initState() {
    Provider.of<GraphPageConrtoller>(context, listen: false)
        .getCategoryTransactionList(widget.category, context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            getIcon(widget.category),
            const SizedBox(
              width: 20,
            ),
            Text(
              getCategoryName(widget.category),
              style: GoogleFonts.poppins(
                fontSize: 26,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
            const SizedBox(
              width: 40,
            ),
          ],
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.black,
            weight: 15,
          ),
        ),
        backgroundColor: getColor(widget.category),
      ),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () async {
          return Provider.of<GraphPageConrtoller>(context, listen: false)
              .getCategoryTransactionList(widget.category, context);
        },
        color: getColor(widget.category),
        child: Consumer<GraphPageConrtoller>(
          builder: (context, value, child) {
            return ListView.builder(
              itemCount: Provider.of<GraphPageConrtoller>(context)
                  .categoryTransactionList
                  .length,
              itemBuilder: (context, index) {
                return getSingleTransactionContainer(
                  transaction: Provider.of<GraphPageConrtoller>(context)
                      .categoryTransactionList[index],
                );
              },
            );
          },
        ),
      ),
    );
  }

  Widget getIconWithCircle(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    List<IconData> icons = [
      Icons.food_bank_outlined,
      Icons.local_gas_station_rounded,
      Icons.medical_services_outlined,
      Icons.movie_creation_outlined,
      Icons.shopping_bag_outlined,
    ];

    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: colors[category - 1],
      ),
      padding: const EdgeInsets.all(8),
      child: Icon(
        icons[category - 1],
        color: Colors.white,
      ),
    );
  }

  Widget getSingleTransactionContainer(
      {required SingleCategoryTransaction transaction}) {
    return GestureDetector(
      onTap: () => showInfoDialog(context, transaction),
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        transaction.description,
                        style: GoogleFonts.poppins(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        transaction.amount.toString(),
                        style: GoogleFonts.poppins(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      transaction.datetime,
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Divider(),
          ],
        ),
      ),
    );
  }

  void showInfoDialog(
      BuildContext context, SingleCategoryTransaction transaction) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: Row(
            children: [
              getIconWithCircle(transaction.category),
              const SizedBox(
                width: 20,
              ),
              Text(
                getCategoryName(transaction.category),
                style: GoogleFonts.poppins(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                transaction.description,
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Text(transaction.datetime),
              ),
            ],
          ),
        );
      },
    );
  }
}
