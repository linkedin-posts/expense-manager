import 'package:expense_manager/pages/category_transactions_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Categorypage extends StatelessWidget {
  const Categorypage({super.key});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.1,
      ),
      itemCount: 5,
      itemBuilder: (context, index) {
        return getSingleContainer(index + 1, context);
      },
    );
  }

  Widget getSingleContainer(int index, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return CategoryTransactionsPage(category: index);
            },
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: getColor(index),
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 8,
            )
          ],
        ),
        margin: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            getIcon(index),
            Text(
              getCategory(index),
              style: GoogleFonts.poppins(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      ),
    );
  }

  Color getColor(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    return colors[category - 1];
  }

  Widget getIcon(int category) {
    List<IconData> icons = [
      Icons.food_bank_outlined,
      Icons.local_gas_station_rounded,
      Icons.medical_services_outlined,
      Icons.movie_creation_outlined,
      Icons.shopping_bag_outlined,
    ];

    return Icon(
      icons[category - 1],
      color: Colors.white,
      size: 70,
    );
  }

  String getCategory(int category) {
    if (category == 1) {
      return "Food";
    } else if (category == 2) {
      return "Fuel";
    } else if (category == 3) {
      return "Mediciene";
    } else if (category == 4) {
      return "Entertainment";
    } else if (category == 5) {
      return "Shopping";
    } else {
      return "Other";
    }
  }
}
