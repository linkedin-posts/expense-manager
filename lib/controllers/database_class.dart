import 'dart:developer';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MyDatabase {
  static late final Database database;

  static Future initDataBase() async {
    var path = await getDatabasesPath();
    database = await openDatabase(
      join(path, 'expense_manager.db'),
      version: 2,
      onCreate: (d, version) {
        d.execute(
          "CREATE TABLE IF NOT EXISTS expense (id INTEGER PRIMARY KEY, month INTEGER , year INTEGER, day INTEGER , description TEXT, datetime TEXT, category INTEGER, amount REAL)",
        );
        d.execute(
          "CREATE TABLE IF NOT EXISTS trash (id INTEGER PRIMARY KEY, month INTEGER , year INTEGER, day INTEGER , description TEXT, datetime TEXT, category INTEGER, amount REAL)",
        );

        log("Created Table");
      },
    );
  }
}
