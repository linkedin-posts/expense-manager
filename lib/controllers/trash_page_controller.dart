import 'dart:developer';

import 'package:expense_manager/controllers/database_class.dart';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TrashPageController with ChangeNotifier {
  List<SingleTransaction> transactionList = [];

  Future<int> getLastTrashId() async {
    try {
      final db = MyDatabase.database;

      List<Map<String, dynamic>> result =
          await db.rawQuery('SELECT id FROM trash ORDER BY id DESC LIMIT 1');

      if (result.isNotEmpty) {
        return result.first['id'];
      } else {
        return 0;
      }
    } catch (e) {
      log('Exception in getLastTrashId: $e');
      return 0;
    }
  }

  void getList(BuildContext context) async {
    try {
      transactionList.clear();
      final db = MyDatabase.database;

      int month = Provider.of<HomePageController>(context, listen: false).month;
      int year = Provider.of<HomePageController>(context, listen: false).year;

      List<Map<String, dynamic>> tempList = await db.rawQuery(
          "SELECT * FROM trash WHERE month = $month AND year = $year ORDER BY day");


      for (var element in tempList) {
        transactionList.add(
          SingleTransaction(
            id: element['id'],
            day: element['day'],
            amount: element['amount'],
            category: element['category'],
            datetime: element['datetime'],
            description: element['description'],
            month: element['month'],
            year: element['year'],
          ),
        );
      }
    } catch (e) {
      log('Exception in getList: $e');
    }
    notifyListeners();
  }

  Future<void> deleteFromTrash(int id, BuildContext context) async {
    try {
      final db = MyDatabase.database;

      await db.rawDelete("DELETE FROM trash WHERE id=$id");

      getList(context);
    } catch (e) {
      log('Exception in getLastTrashId: $e');
    }
  }
}

class SingleTransaction {
  int id;
  int month;
  int year;
  int day;
  int category;
  String description;
  String datetime;
  double amount;

  SingleTransaction({
    required this.id,
    required this.year,
    required this.day,
    required this.amount,
    required this.category,
    required this.datetime,
    required this.description,
    required this.month,
  });
}
