import 'dart:developer';

import 'package:expense_manager/controllers/database_class.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class CategoryPageController with ChangeNotifier {
  List<SingleCategoryTransaction> categoryTransactionList = [];

  Future<void> getCategoryTransactionList(
      int category) async {
    try {
      categoryTransactionList.clear();
      Database db = MyDatabase.database;

      List<Map<String, dynamic>> tempList = await db.rawQuery(
          "SELECT * FROM expense WHERE category = $category ORDER BY year, month, day");

      for (var element in tempList) {
        categoryTransactionList.add(
          SingleCategoryTransaction(
            amount: element['amount'],
            category: category,
            datetime: element['datetime'],
            description: element['description'],
            month: element['month'],
            id: element['id'],
            year: element['year'],
            day: element['day'],
          ),
        );
      }
      notifyListeners();
    } catch (e) {
      log(e.toString());
    }
  }
}

class SingleCategoryTransaction {
  int id;
  int month;
  int year;
  int day;
  int category;
  String description;
  String datetime;
  double amount;

  SingleCategoryTransaction({
    required this.id,
    required this.amount,
    required this.year,
    required this.day,
    required this.category,
    required this.datetime,
    required this.description,
    required this.month,
  });
}
