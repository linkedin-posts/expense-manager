import 'dart:developer';

import 'package:expense_manager/controllers/graph_page_controller.dart';
import 'package:expense_manager/controllers/transaction_page_controller.dart';
import 'package:expense_manager/controllers/trash_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';

class HomePageController with ChangeNotifier {
  int index = 0;
  String monthName = "";
  int month = 0;
  int year = 0;

  HomePageController() {
    DateTime now = DateTime.now();
    monthName = DateFormat.yMMMM().format(now);
    month = now.month;
    year = now.year;
    notifyListeners();
  }

  void changeIndex(int newIndex) async {
    index = newIndex;
    notifyListeners();
  }

  Future<void> changeMonth(BuildContext context) async {
    try {
      DateTime? dateTime = await showMonthPicker(
        context: context,
        initialDate: DateTime.now(),
        headerColor: const Color.fromRGBO(14, 161, 125, 1),
        backgroundColor: Colors.white,
        selectedMonthBackgroundColor: const Color.fromRGBO(14, 161, 125, 1),
        unselectedMonthTextColor: Colors.black,
        firstDate: DateTime(2000),
        lastDate: DateTime(3000),
      );

      if (dateTime != null) {
        monthName = DateFormat.yMMMM().format(dateTime);
        month = int.parse(DateFormat.M().format(dateTime));
        Provider.of<TransactionPageController>(context, listen: false)
            .getList(context);
        Provider.of<GraphPageConrtoller>(context, listen: false)
            .setCategoryExpenseMap(context);

        Provider.of<TrashPageController>(context, listen: false)
            .getList(context);

        notifyListeners();
      }
    } catch (e) {
      log('Exception in demofun: $e');
    }
  }
}
