import 'dart:developer';

import 'package:expense_manager/controllers/database_class.dart';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

class GraphPageConrtoller with ChangeNotifier {
  double food = 0;
  double fuel = 0;
  double medicine = 0;
  double entertainment = 0;
  double shoping = 0;
  double total = 0;

  List<SingleCategoryTransaction> categoryTransactionList = [];

  void setCategoryExpenseMap(BuildContext context) async {
    try {
      total = 0;
      food = 0;
      fuel = 0;
      medicine = 0;
      entertainment = 0;
      shoping = 0;

      Database db = MyDatabase.database;

      int month = Provider.of<HomePageController>(context, listen: false).month;
      int year = Provider.of<HomePageController>(context, listen: false).year;

      List<Map<String, dynamic>> data = await db.rawQuery(
          "SELECT amount,category FROM expense WHERE month=$month AND year=$year");

      for (var element in data) {
        if (element['category'] == 1) {
          food = food + element['amount'];
        } else if (element['category'] == 2) {
          fuel = fuel + element['amount'];
        } else if (element['category'] == 3) {
          medicine = medicine + element['amount'];
        } else if (element['category'] == 4) {
          entertainment = entertainment + element['amount'];
        } else if (element['category'] == 5) {
          shoping = shoping + element['amount'];
        }
        total = total + element['amount'];
      }

      notifyListeners();
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> getCategoryTransactionList(
      int category, BuildContext context) async {
    try {
      categoryTransactionList.clear();
      Database db = MyDatabase.database;

      int month = Provider.of<HomePageController>(context, listen: false).month;
      int year = Provider.of<HomePageController>(context, listen: false).year;

      List<Map<String, dynamic>> tempList = await db.rawQuery(
          "SELECT * FROM expense WHERE category = $category AND month = $month AND year = $year ORDER BY day");

      for (var element in tempList) {
        categoryTransactionList.add(
          SingleCategoryTransaction(
            amount: element['amount'],
            category: category,
            datetime: element['datetime'],
            description: element['description'],
            month: element['month'],
            id: element['id'],
            year: element['year'],
            day: element['day'],
          ),
        );
      }
      notifyListeners();
    } catch (e) {
      log(e.toString());
    }
  }
}

class SingleCategoryTransaction {
  int id;
  int month;
  int year;
  int day;
  int category;
  String description;
  String datetime;
  double amount;

  SingleCategoryTransaction({
    required this.id,
    required this.amount,
    required this.year,
    required this.day,
    required this.category,
    required this.datetime,
    required this.description,
    required this.month,
  });
}
