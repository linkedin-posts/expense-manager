import 'dart:developer';
import 'package:expense_manager/controllers/database_class.dart';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

class TransactionPageController with ChangeNotifier {
  List<SingleTransaction> transactionList = [];

  Future<bool> addTransaction({
    required int category,
    required int month,
    required int year,
    required int day,
    required String description,
    required String datetime,
    required double amount,
    required BuildContext context,
  }) async {
    try {
      final db = MyDatabase.database;
      int id = await getLastId();
      id++;
      await db.rawInsert(
        'INSERT INTO expense(id,category,month,year,day,description,datetime,amount) VALUES($id, $category, $month , $year, $day, "$description", "$datetime", $amount)',
      );
      await getList(context);
      notifyListeners();
      return true;
    } catch (e) {
      log('Exception in addTransaction: $e');
    }
    return false;
  }

  Future<void> moveToTrash(int id, BuildContext context) async {
    try {
      Database db = MyDatabase.database;
      List<Map<String, dynamic>> tempObject =
          await db.rawQuery("SELECT * FROM expense WHERE id=$id");
      if (tempObject.isEmpty) {
        log('No data found with id: $id');
        return;
      }
      int trashId = await getLastTrashId();
      trashId++;
      int category = tempObject[0]['category'];
      int month = tempObject[0]['month'];
      int year = tempObject[0]['year'];
      int day = tempObject[0]['day'];
      String description = tempObject[0]['description'];
      String datetime = tempObject[0]['datetime'];
      double amount = tempObject[0]['amount'];
      await db.rawInsert(
        'INSERT INTO trash (id, category, month, year, day, description, datetime, amount) VALUES ($trashId, $category, $month, $year, $day, "$description", "$datetime", $amount)',
      );
      await db.rawDelete("DELETE FROM expense WHERE id = ?", [id]);
      await getList(context);
      notifyListeners();
    } catch (e) {
      log('Exception in moveToTrash: $e');
    }
  }

  Future<int> getLastTrashId() async {
    try {
      final db = MyDatabase.database;
      List<Map<String, dynamic>> result =
          await db.rawQuery('SELECT id FROM trash ORDER BY id DESC LIMIT 1');
      if (result.isNotEmpty) {
        return result.first['id'];
      } else {
        return 0;
      }
    } catch (e) {
      log('Exception in getLastTrashId: $e');
      return 0;
    }
  }

  Future<void> getList(BuildContext context) async {
    try {
      final month =
          Provider.of<HomePageController>(context, listen: false).month;
      final year = Provider.of<HomePageController>(context, listen: false).year;
      transactionList.clear();
      final db = MyDatabase.database;
      List<Map<String, dynamic>> tempList = await db.rawQuery(
        'SELECT * FROM expense WHERE month = ? AND year = ? ORDER BY day',
        [month, year],
      );

      for (var element in tempList) {
        transactionList.add(
          SingleTransaction(
            id: element['id'],
            day: element['day'],
            amount: element['amount'],
            category: element['category'],
            datetime: element['datetime'],
            description: element['description'],
            month: element['month'],
            year: element['year'],
          ),
        );
      }
      notifyListeners();
    } catch (e) {
      log('Exception in getList: $e');
    }
  }

  Future<int> getLastId() async {
    try {
      final db = MyDatabase.database;
      List<Map<String, dynamic>> result =
          await db.rawQuery('SELECT id FROM expense ORDER BY id DESC LIMIT 1');
      if (result.isNotEmpty) {
        return result.first['id'];
      } else {
        return 0;
      }
    } catch (e) {
      log('Exception in getLastId: $e');
      return 0;
    }
  }
}

class SingleTransaction {
  int id;
  int month;
  int year;
  int day;
  int category;
  String description;
  String datetime;
  double amount;

  SingleTransaction({
    required this.id,
    required this.year,
    required this.day,
    required this.amount,
    required this.category,
    required this.datetime,
    required this.description,
    required this.month,
  });
}
