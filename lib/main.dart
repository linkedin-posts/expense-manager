import 'package:expense_manager/controllers/category_page_controller.dart';
import 'package:expense_manager/controllers/database_class.dart';
import 'package:expense_manager/controllers/graph_page_controller.dart';
import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:expense_manager/controllers/transaction_page_controller.dart';
import 'package:expense_manager/controllers/trash_page_controller.dart';
import 'package:expense_manager/firebase_options.dart';
import 'package:expense_manager/login/login_controller.dart';
import 'package:expense_manager/login/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  Provider.debugCheckInvalidValueType = null;
  await MyDatabase.initDataBase();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) {
            return HomePageController();
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return TransactionPageController();
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return GraphPageConrtoller();
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return TrashPageController();
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return CategoryPageController();
          },
        ),
        Provider(
          create: (context) {
            return LoginController();
          },
        ),
      ],
      child: const MaterialApp(
        title: 'Expense Manager',
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
      ),
    );
  }
}
