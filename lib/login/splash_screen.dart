import 'package:expense_manager/login/registor_page.dart';
import 'package:expense_manager/pages/home_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      Duration(seconds: 3),
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => FirebaseAuth.instance.currentUser != null
                ? HomePage()
                : RegistorPage(),
          ),
        );
      },
    );

    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 6,
              child: Container(
                height: 250,
                width: 250,
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(234, 238, 235, 1),
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/expense_manger.png",
                  height: 500,
                ),
              ),
            ),
            Expanded(
              child: Text(
                "Expense Manager",
                style: GoogleFonts.poppins(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
