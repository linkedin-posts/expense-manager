import 'package:flutter/material.dart';

class LoginTextField extends StatefulWidget {
  final String hintText;
  final TextEditingController controller;
  final IconData iconData;
  final bool isPasswordField;
  const LoginTextField({
    required this.hintText,
    required this.controller,
    required this.iconData,
    this.isPasswordField = false,
    super.key,
  });

  @override
  State<LoginTextField> createState() => _LoginTextFieldState();
}

class _LoginTextFieldState extends State<LoginTextField> {
  bool isObsecure = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            blurRadius: 10,
            color: Color.fromRGBO(0, 0, 0, 0.15),
          ),
        ],
      ),
      margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 12),
      padding: const EdgeInsets.only(
        left: 15,
        right: 5,
        top: 5,
        bottom: 5,
      ),
      child: TextField(
        controller: widget.controller,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          prefixIcon: Icon(
            widget.iconData,
            color: Color.fromRGBO(14, 161, 125, 1),
          ),
          border: InputBorder.none,
          hintText: widget.hintText,
          suffixIcon: widget.isPasswordField
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      isObsecure = !isObsecure;
                    });
                  },
                  child: Icon(
                    isObsecure ? Icons.visibility : Icons.visibility_off,
                    color: Color.fromRGBO(14, 161, 125, 0.6),
                  ),
                )
              : null,
        ),
        obscureText: widget.isPasswordField ? isObsecure : false,
      ),
    );
  }
}
