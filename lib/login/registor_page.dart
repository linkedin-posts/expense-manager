import 'package:expense_manager/common/waiting_circle.dart';
import 'package:expense_manager/login/login_button.dart';
import 'package:expense_manager/login/login_controller.dart';
import 'package:expense_manager/login/login_page.dart';
import 'package:expense_manager/login/login_textfield.dart';
import 'package:expense_manager/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class RegistorPage extends StatelessWidget {
  const RegistorPage({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController confirmPasswordController = TextEditingController();

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SizedBox(
        width: double.infinity,
        child: ListView(
          children: [
            Image.asset(
              "assets/expense_manger.png",
              height: 250,
            ),
            const SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 30),
                child: Text(
                  "Create your Account",
                  style: GoogleFonts.poppins(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            LoginTextField(
              hintText: "Name",
              controller: nameController,
              iconData: Icons.person,
            ),
            LoginTextField(
              hintText: "Email",
              controller: emailController,
              iconData: Icons.email,
            ),
            LoginTextField(
              hintText: "Password",
              controller: passwordController,
              iconData: Icons.lock,
              isPasswordField: true,
            ),
            LoginTextField(
              hintText: "Confirm Password",
              controller: confirmPasswordController,
              iconData: Icons.lock,
              isPasswordField: true,
            ),
            const SizedBox(
              height: 20,
            ),
            LoginButton(
              text: "Sign Up",
              onTap: () async {
                String name = nameController.text;
                String email = emailController.text;
                String password = passwordController.text;
                String confirmPassword = confirmPasswordController.text;

                if (name.isEmpty ||
                    email.isEmpty ||
                    password.isEmpty ||
                    confirmPassword.isEmpty) {
                  showLoginSnakBar("Please Enter All Details", context);
                  return;
                }

                WaitingCircle.showWaitingCircle(context);

                if (password != confirmPassword) {
                  showLoginSnakBar("Please Enter Same Password", context);
                } else {
                  bool response =
                      await Provider.of<LoginController>(context, listen: false)
                          .registorUser(name, email, password);

                  WaitingCircle.stopWaitingCircle(context);

                  if (response) {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (context) {
                          return HomePage();
                        },
                      ),
                    );
                  } else {
                    showLoginSnakBar("Email or Password is Incorrect", context);
                  }
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Already have an account?",
                  style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return LoginPage();
                        },
                      ),
                    );
                  },
                  child: Text(
                    " Sign In",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(14, 161, 125, 1),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
