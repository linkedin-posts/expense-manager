import 'package:expense_manager/controllers/transaction_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class SingleContainerHomePage extends StatelessWidget {
  final SingleTransaction transaction;
  const SingleContainerHomePage({required this.transaction, super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showInfoDialog(context),
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      getIcon(transaction.category),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  getCategory(transaction.category),
                                  style: GoogleFonts.poppins(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.do_disturb_on_rounded,
                                  color: Colors.deepOrange,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  transaction.amount.toString(),
                                  style: GoogleFonts.poppins(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              transaction.description.toString(),
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Text(
                                transaction.datetime.toString(),
                                style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Divider(
              color: Color.fromRGBO(206, 206, 206, 1),
            ),
          ],
        ),
      ),
    );
  }

  void showInfoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // backgroundColor: getColor(transaction.category),
          backgroundColor: Colors.white,

          title: Row(
            children: [
              getIcon(transaction.category),
              const SizedBox(
                width: 20,
              ),
              Text(
                getCategory(transaction.category),
                style: GoogleFonts.poppins(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                transaction.description,
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Text(transaction.datetime),
              ),
            ],
          ),

          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
                Provider.of<TransactionPageController>(
                  context,
                  listen: false,
                ).moveToTrash(
                  transaction.id,
                  context,
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Text(
                    "Delete",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red.withOpacity(0.7),
                elevation: 5,
              ),
            ),
          ],
        );
      },
    );
  }

  Color getColor(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    return colors[category - 1];
  }

  Widget getIcon(int category) {
    List<Color> colors = [
      const Color.fromRGBO(214, 3, 3, 0.7),
      const Color.fromRGBO(241, 38, 196, 0.7),
      const Color.fromRGBO(0, 174, 91, 0.7),
      const Color.fromRGBO(100, 62, 255, 0.7),
      const Color.fromRGBO(0, 148, 255, 0.7),
    ];

    List<IconData> icons = [
      Icons.food_bank_outlined,
      Icons.local_gas_station_rounded,
      Icons.medical_services_outlined,
      Icons.movie_creation_outlined,
      Icons.shopping_bag_outlined,
    ];

    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: colors[category - 1],
      ),
      padding: const EdgeInsets.all(8),
      child: Icon(
        icons[category - 1],
        color: Colors.white,
      ),
    );
  }

  String getCategory(int category) {
    if (category == 1) {
      return "Food";
    } else if (category == 2) {
      return "Fuel";
    } else if (category == 3) {
      return "Mediciene";
    } else if (category == 4) {
      return "Entertainment";
    } else if (category == 5) {
      return "Shopping";
    } else {
      return "Other";
    }
  }
}
