import 'package:flutter/material.dart';

class WaitingCircle {
  static void showWaitingCircle(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Center(
            child: SizedBox(
              height: 150,
              width: 150,
              child: CircularProgressIndicator(
                color: const Color.fromRGBO(14, 161, 125, 1),
              ),
            ),
          ),
        );
      },
    );
  }

  static void stopWaitingCircle(BuildContext context) {
    Navigator.of(context).pop();
  }
}
