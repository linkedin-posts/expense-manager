import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CategoryDropDownButton extends StatefulWidget {
  final ValueChanged<int?> fun;
  const CategoryDropDownButton({required this.fun, super.key});

  @override
  State<CategoryDropDownButton> createState() => _CategoryDropDownButtonState();
}

class _CategoryDropDownButtonState extends State<CategoryDropDownButton> {
  int? selectedVlaue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      value: selectedVlaue,
      items: [
        getItem(
          name: "Food",
          value: 1,
          color: const Color.fromRGBO(214, 3, 3, 0.4),
        ),
        getItem(
          name: "Fuel",
          value: 2,
          color: const Color.fromRGBO(241, 38, 196, 0.4),
        ),
        getItem(
          name: "Medicine",
          value: 3,
          color: const Color.fromRGBO(0, 174, 91, 0.4),
        ),
        getItem(
          name: "Entertainment",
          value: 4,
          color: const Color.fromRGBO(100, 62, 255, 0.4),
        ),
        getItem(
          name: "Shopping",
          value: 5,
          color: const Color.fromRGBO(0, 148, 255, 0.4),
        )
      ],
      onChanged: (value) {
        setState(() {
          selectedVlaue = value;
        });

        widget.fun(value);
      },
      hint: Text(
        "Select Category",
        style: GoogleFonts.poppins(
          fontSize: 17,
          fontWeight: FontWeight.w600,
        ),
      ),
      dropdownColor: Colors.grey.shade50,
      isExpanded: true,
    );
  }

  DropdownMenuItem getItem(
      {required String name, required int value, required Color color}) {
    return DropdownMenuItem<int>(
      value: value,
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              name,
              style: GoogleFonts.poppins(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
