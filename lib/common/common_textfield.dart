import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CommonTextfield extends StatefulWidget {
  String hint;
  TextEditingController controller;
  bool isDateField = false;
  int maxLines = 1;
  bool isNumber = true;
  CommonTextfield({
    required this.hint,
    required this.controller,
    this.isDateField = false,
    this.maxLines = 1,
    this.isNumber = false,
    super.key,
  });

  @override
  State<CommonTextfield> createState() => _CommonTextfieldState();
}

class _CommonTextfieldState extends State<CommonTextfield> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
      child: TextField(
        readOnly: widget.isDateField,
        controller: widget.controller,
        textInputAction: TextInputAction.next,
        keyboardType: widget.isNumber
            ? TextInputType.number
            : TextInputType.visiblePassword,
        onTap: widget.isDateField
            ? () async {
                DateTime? dateTime = await showDatePicker(
                  context: context,
                  firstDate: DateTime(2000),
                  lastDate: DateTime(3000),
                  initialDate: DateTime.now(),
                );

                String str = DateFormat.yMMMMd().format(dateTime!);
                setDate(str);
              }
            : null,
        maxLines: widget.maxLines,
        decoration: InputDecoration(
          labelText: widget.hint,
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              color: Color.fromRGBO(14, 161, 125, 1),
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              color: Color.fromRGBO(14, 161, 125, 1),
            ),
          ),
        ),
      ),
    );
  }

  void setDate(String str) {
    setState(() {
      widget.controller.text = str;
    });
    log(widget.controller.text);
  }
}
