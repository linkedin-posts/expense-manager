import 'dart:developer';

import 'package:expense_manager/common/category_drop_down.dart';
import 'package:expense_manager/common/common_button.dart';
import 'package:expense_manager/common/common_textfield.dart';
import 'package:expense_manager/controllers/transaction_page_controller.dart';
import 'package:expense_manager/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddTaskScreen extends StatefulWidget {
  const AddTaskScreen({super.key});

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  int category = 1;
  late TextEditingController dateController;
  late TextEditingController amountController;
  late TextEditingController descriptionController;

  @override
  void initState() {
    dateController = TextEditingController();
    amountController = TextEditingController();
    descriptionController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            CommonTextfield(
              hint: "Date",
              controller: dateController,
              isDateField: true,
            ),
            CommonTextfield(
              hint: "Amount",
              controller: amountController,
              isNumber: true,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
                top: 5,
                bottom: 5,
              ),
              child: SizedBox(
                width: double.infinity,
                child: CategoryDropDownButton(
                  fun: getData,
                ),
              ),
            ),
            CommonTextfield(
              hint: "Description",
              controller: descriptionController,
              maxLines: 3,
            ),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.center,
              child: CommonButton(
                text: "Add",
                onPressed: () {
                  DateFormat dateFormat = DateFormat("MMMM d, yyyy");

                  DateTime dateTime = dateFormat.parse(dateController.text);

                  int month = dateTime.month;
                  int year = dateTime.year;
                  int day = dateTime.day;

                  Provider.of<TransactionPageController>(context, listen: false)
                      .addTransaction(
                    category: category,
                    context: context,
                    year: year,
                    month: month,
                    day : day,
                    description: descriptionController.text,
                    datetime: dateController.text,
                    amount: double.parse(amountController.text),
                  );
                  Navigator.of(context).pop();
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  void getData(int? value) {
    category = value!;
  }
}
