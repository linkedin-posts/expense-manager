import 'package:expense_manager/home_page_widgets/add_task_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FloatingActionButtonHomePage extends StatelessWidget {
  const FloatingActionButtonHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      onPressed: () {
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (context) {
            return AddTaskScreen();
          },
        );
      },
      backgroundColor: Colors.white,
      label: Row(
        children: [
          Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Color.fromRGBO(14, 161, 125, 1),
            ),
            child: const Icon(
              Icons.add,
              color: Colors.white,
              size: 35,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Text(
            "Add Transaction",
            style: GoogleFonts.poppins(
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          )
        ],
      ),
    );
  }
}
