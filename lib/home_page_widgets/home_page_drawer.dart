import 'dart:developer';

import 'package:expense_manager/controllers/home_page_controller.dart';
import 'package:expense_manager/main.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class HomePageDrawer extends StatelessWidget {
  const HomePageDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Text(
            "Expense Manager",
            style: GoogleFonts.poppins(
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            "Saves all your Transactions",
            style: GoogleFonts.poppins(
              color: Colors.black.withOpacity(0.5),
              fontSize: 15,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          getContainer(
            text: "Transaction",
            iconData: Icons.my_library_books_outlined,
            context: context,
            index: 0,
          ),
          getContainer(
            text: "Graphs",
            iconData: Icons.pie_chart_rounded,
            context: context,
            index: 1,
          ),
          getContainer(
            text: "Category ",
            iconData: Icons.category_rounded,
            context: context,
            index: 2,
          ),
          getContainer(
            text: "Trash ",
            iconData: Icons.delete_outline_outlined,
            context: context,
            index: 3,
          ),
          getContainer(
            text: "About us",
            iconData: Icons.person,
            context: context,
            index: 4,
          ),
        ],
      ),
    );
  }

  Widget getContainer({
    required String text,
    required IconData iconData,
    required BuildContext context,
    required int index,
  }) {
    bool isSelected = Provider.of<HomePageController>(context).index == index;
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
        Provider.of<HomePageController>(context, listen: false)
            .changeIndex(index);
      },
      child: Container(
        decoration: BoxDecoration(
          color: isSelected ? const Color.fromRGBO(14, 161, 125, 0.15) : null,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        margin: const EdgeInsets.only(right: 30, top: 10, bottom: 10),
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            const SizedBox(
              width: 10,
            ),
            Icon(
              iconData,
              size: 35,
              color: const Color.fromRGBO(14, 161, 125, 1),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  text,
                  style: GoogleFonts.poppins(
                    color: const Color.fromRGBO(14, 161, 125, 1),
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
